from django.urls import path

from status.views import status_check

urlpatterns = [
    path("", status_check),
]
