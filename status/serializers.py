from rest_framework import serializers


class StatusSerializer(serializers.Serializer[None]):
    status = serializers.CharField()
