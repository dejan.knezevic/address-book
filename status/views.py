"""
Status endpoint can be used for health-checks to make sure the service is running properly.
"""
from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.request import Request
from rest_framework.response import Response

from status.serializers import StatusSerializer


@extend_schema(request=None, responses=StatusSerializer)
@api_view(["GET"])
def status_check(_: Request) -> Response:
    """
    Status endpoint used for healthcheck.
    Returns successful status response if service is up.
    """
    return Response({"status": "OK"}, status=status.HTTP_200_OK)
