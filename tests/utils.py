from urllib.parse import quote_plus, urlencode


def with_query_params(url, params=None):
    params = params or {}
    return f"{url}?{urlencode(params, quote_via=quote_plus)}"
