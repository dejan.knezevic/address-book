import pytest
from faker import Faker

fake = Faker()


@pytest.mark.django_db
def test_login(given, client, verify):
    password = fake.password()
    user = given.user.exists(password=password)

    response = client.post(
        path="/authentication/login/",
        data={"username": user.username, "password": password},
    )

    verify.http_response.status(response=response, status_code=200)
    assert "token" in response.data


@pytest.mark.django_db
def test_logout(given, client, verify):
    user = given.user.exists()
    given.user.logged_in(user=user, client=client)

    response = client.post(path="/authentication/logout/")

    verify.http_response.status(response=response, status_code=200)
