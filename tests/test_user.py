import pytest
from faker import Faker

fake = Faker()


@pytest.mark.django_db
def test_user_create(given, verify):
    user = given.user.exists()
    verify.user.saved(user=user)
