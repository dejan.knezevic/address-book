from django.contrib.auth.models import User
from faker import Faker
from rest_framework.authtoken.models import Token

from users.models import Address

fake = Faker()


class Preconditions:
    def __init__(self):
        self.user = UserPrecondition
        self.address = AddressPrecondition


class UserPrecondition:
    @staticmethod
    def exists(username=None, password=None):
        user = User.objects.create_user(
            username=username or fake.slug(),
            password=password or fake.password(),
        )
        user.save()
        return user

    @staticmethod
    def logged_in(user, client):
        token = Token(user=user)
        token.save()
        client.credentials(HTTP_AUTHORIZATION=f"Token {token.key}")
        return token


class AddressPrecondition:
    @staticmethod
    def exists(
        user, street=None, number=None, city=None, postal_code=None, country=None
    ):
        address = Address(
            street=street or fake.street_name(),
            number=number or f"{fake.random_digit_not_null()}",
            city=city or fake.city(),
            postal_code=postal_code or fake.postcode(),
            country=country or fake.country(),
            user=user,
        )
        address.save()
        return address
