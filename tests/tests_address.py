from itertools import chain, combinations

import pytest
from faker import Faker

from address_book.errors import BadRequest
from tests.utils import with_query_params

fake = Faker()


@pytest.mark.django_db
def test_address_create(given, verify):
    user = given.user.exists()
    address = given.address.exists(user=user)

    verify.user.saved(user=user)
    verify.address.saved(address_id=address.id)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "number_of_addresses, results_per_page",
    [
        (10, 100),
        (255, 100),
    ],
)
def test_get_addresses(given, client, verify, number_of_addresses, results_per_page):
    user = given.user.exists()
    other_user = given.user.exists()
    for i in range(number_of_addresses):
        given.address.exists(user=user)
    for i in range(number_of_addresses):
        given.address.exists(user=other_user)

    given.user.logged_in(user=user, client=client)

    offset = 0
    results = []
    while offset < number_of_addresses:
        response = client.get(
            path=with_query_params(
                url="/users/address/",
                params={"limit": results_per_page, "offset": offset},
            )
        )
        page = response.data.get("results")
        results.extend(page)
        offset += results_per_page

        verify.http_response.status(response, status_code=200)
        assert len(page) <= results_per_page

    assert len(results) == number_of_addresses


@pytest.mark.django_db
@pytest.mark.parametrize(
    "number_of_addresses, street, number, city, postal_code, country",
    [
        (10, "street", "", "", "", ""),
        (10, "street", "1", "", "", ""),
        (10, "street", "1", "City", "", ""),
        (10, "street", "1", "City", "123", ""),
        (10, "street", "1", "City", "123", "Country"),
    ],
)
def test_get_filtered_addresses(
    given,
    client,
    verify,
    number_of_addresses,
    street,
    number,
    city,
    postal_code,
    country,
):
    user = given.user.exists()
    other_user = given.user.exists()
    for i in range(number_of_addresses):
        given.address.exists(user=user)
    for i in range(number_of_addresses):
        given.address.exists(user=other_user)
    given.address.exists(
        street=street,
        number=number,
        city=city,
        postal_code=postal_code,
        country=country,
        user=user,
    )
    given.address.exists(
        street=street,
        number=number,
        city=city,
        postal_code=postal_code,
        country=country,
        user=other_user,
    )
    given.user.logged_in(user=user, client=client)

    response = client.get(
        path=with_query_params(
            url="/users/address/",
            params={
                "street": street,
                "number": number,
                "city": city,
                "postal_code": postal_code,
                "country": country,
            },
        )
    )

    verify.http_response.status(response, status_code=200)
    assert len(response.data.get("results", [])) == 1


@pytest.mark.django_db
def test_create_address(given, client, verify):
    user = given.user.exists()
    other_user = given.user.exists()

    given.user.logged_in(user=user, client=client)

    # Set other user to already has this address
    address = given.address.exists(user=other_user)

    # Addresses are tied to users, so this will work
    response = client.post(
        "/users/address/",
        data={
            "street": address.street,
            "number": address.number,
            "city": address.city,
            "postal_code": address.postal_code,
            "country": address.country,
        },
    )
    verify.http_response.status(response=response, status_code=201)
    verify.address.saved(address_id=response.data.get("id"))

    # Adding the same address to the same user should not be allowed
    second_response = client.post(
        "/users/address/",
        data={
            "street": address.street,
            "number": address.number,
            "city": address.city,
            "postal_code": address.postal_code,
            "country": address.country,
        },
    )

    verify.http_response.status(
        response=second_response, status_code=BadRequest.status_code
    )


@pytest.mark.django_db
def test_get_address(given, client, verify):
    user = given.user.exists()
    given.user.logged_in(user=user, client=client)
    address = given.address.exists(user=user)

    response = client.get(f"/users/address/{address.id}/")

    verify.http_response.status(response=response, status_code=200)
    assert response.data.get("id") == str(address.id)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "partial",
    [[False], [True]],
)
def test_update_address(given, client, verify, partial):
    user = given.user.exists()
    given.user.logged_in(user=user, client=client)
    address = given.address.exists(user=user)

    params = [
        {"street": address.street},
        {"number": address.number},
        {"city": address.city},
        {"postal_code": address.postal_code},
        {"country": address.country},
    ]

    if partial:
        # Go through all possible combinations of parameters for partial update
        list_combinations = []
        for n in range(len(params) + 1):
            list_combinations.extend(list(combinations(params, n)))
        data_combinations = [
            dict(chain(*map(dict.items, c))) for c in list_combinations
        ]

        for data_combination in data_combinations:
            response = client.patch(
                f"/users/address/{address.id}/", data=data_combination
            )
            verify.http_response.status(response=response, status_code=200)
            for k, v in data_combination.items():
                assert response.data.get(k) == v
    else:
        # Full update will be checked with all params
        data = dict(chain(*map(dict.items, params)))
        response = client.put(f"/users/address/{address.id}/", data=data)
        verify.http_response.status(response=response, status_code=200)
        for k, v in data.items():
            assert response.data.get(k) == v


@pytest.mark.django_db
def test_delete_address(given, client, verify):
    user = given.user.exists()
    given.user.logged_in(user=user, client=client)
    address = given.address.exists(user=user)

    response = client.delete(f"/users/address/{address.id}/")

    verify.http_response.status(response=response, status_code=204)
    verify.address.deleted(address_id=address.id)


@pytest.mark.django_db
def test_bulk_delete_addresses(given, client, verify):
    number_of_addresses = 10
    user = given.user.exists()
    other_user = given.user.exists()
    given.user.logged_in(user=user, client=client)

    addresses = [given.address.exists(user=user) for _ in range(number_of_addresses)]
    other_addresses = [
        given.address.exists(user=other_user) for _ in range(number_of_addresses)
    ]
    response = client.post(
        "/users/address/bulk_delete/",
        data={"ids": [str(address.id) for address in addresses]},
        format="json",
    )

    verify.http_response.status(response=response, status_code=200)
    for address in addresses:
        verify.address.deleted(address_id=address.id)

    for address in other_addresses:
        verify.address.saved(address_id=address.id)
