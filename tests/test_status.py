def test_status(client, verify):
    response = client.get(path="/status/")
    verify.http_response.status(response=response, status_code=200)
