import pytest
from rest_framework.test import APIClient

from tests.preconditions import Preconditions
from tests.verifiers import Verifiers


@pytest.fixture
def given():
    return Preconditions()


@pytest.fixture
def verify():
    return Verifiers()


@pytest.fixture
def client():
    return APIClient()
