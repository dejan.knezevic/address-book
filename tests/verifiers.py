from django.contrib.auth.models import User

from users.models import Address


class Verifiers:
    def __init__(self):
        self.http_response = HttpResponseVerifier
        self.user = UserVerifier
        self.address = AddressVerifier


class HttpResponseVerifier:
    @staticmethod
    def status(response, status_code):
        assert response.status_code == status_code

    @staticmethod
    def data(response, expected):
        assert response.data == expected


class UserVerifier:
    @staticmethod
    def saved(user: User):
        assert User.objects.filter(username=user.username).first() is not None


class AddressVerifier:
    @staticmethod
    def saved(address_id):
        assert Address.objects.filter(id=address_id).first() is not None

    @staticmethod
    def deleted(address_id):
        assert Address.objects.filter(id=address_id).first() is None
