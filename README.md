# Address book

[![pipeline status](https://gitlab.com/dejan.knezevic/address-book/badges/main/pipeline.svg)](https://gitlab.com/dejan.knezevic/address-book/-/commits/main)
[![coverage report](https://gitlab.com/dejan.knezevic/address-book/badges/main/coverage.svg)](https://gitlab.com/dejan.knezevic/address-book/-/commits/main)
[![Python 3.10+](https://img.shields.io/badge/python-3.10+-blue.svg)](https://www.python.org/downloads/)
[![License](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/dejan.knezevic/address-book/-/blob/main/LICENSE)

Address book is a simple REST API service that enables its users to manipulate their addresses.

## Requirements

- Python 3.10

## Installation

Clone the repository with:
```bash
git clone git@gitlab.com:dejan.knezevic/address-book.git
```

Install development requirements with:
```bash
pip install -r requirements-test.txt
```

## Initialize database for testing

To generate the database, run migrations with:
```bash
python manage.py migrate
```

Basic database records can be generated using a custom management command:
```bash
python manage.py init_db
```
This will create several users as well as the admin user. Administrator's credentials are `admin/admin` and all users
will have `password` set as their password. This will be used for API login.


## Configuration

Several default settings can be overridden using environment variables, otherwise they will use their defaults:
```
DEBUG=true
SECRET_KEY=development_secret_key
APP_VERSION=0.0.1
```

## Running the server

Server can be started for local development using:
```bash
python manage.py runserver
```
This will start a local server on http://127.0.0.1:8000.

## OpenAPI Schema

The service's OpenAPI schema can be accessed on it's root address. This auto-documenting page will allow the user
to easily log in, log out and test all endpoints.

After logging in with the `/authorization/login/` endpoint, the returned token can be used to authenticate the user
by using the `Authorize` button on the top-left of the page. Value should be in the following format:
```
Token <token_uuid>
```

## Docker image

Another simple way to use this service is to build the docker image with:
```bash
docker build -t adress-book .
```
The image can be run with:
```bash
docker run -p 8000:8000 address-book
```

This will expose the server on http://127.0.0.1:8000, just like the approach above (only missing the test data).

## Development

A pre-commit hook is available for this project, it can be installed with:
```bash
pre-commit install
```
It will automatically run all checks before each commit, but can also be run manually on all files with:
```bash
pre-commit run -a
```

## Additional notes

### Database
For this basic use case SQLite was enough since actual deployment is not planned, and it is simpler to test locally,
even with a single session restriction. Otherwise, PostgreSql would be used with the `psycopg2-binary` package for the
database driver.

### Dependencies
The plan was to use Dante to create reproducible lock files, I did not have a chance to update it for a while,
and the last several python versions added a lot of extras and backport packages, making it not work properly.
Both `requirements-test.txt` and `requirements-production.txt` were created manually.

### Logging and events
Logging could also be configured both for service operations and for access and audit logs.
Certain events can be sent to something like Kafka for later processing.

### Data export
For users with a very large number of addresses, something like a csv export could be implemented.

### Other
Any other notes can be found in related files' documentation
