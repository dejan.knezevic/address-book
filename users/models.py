"""
Module for this app's model definition.

The default User model is used instead of an override or one-to-one relation as
any future required modifications can be done by adding a UserDetails type model
which will hold all new user information.
Additional user models can be created to further separate standard users from staff or admins.

The Address model is using several basic fields just to illustrate an example. A proper address
definition would be required here to represent all possible addresses if this were to be used for
an actual project. This would be discussed in a product specification, so there are no
special validations for now.
"""
import uuid

from django.db import models


class Address(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, db_index=True)
    street = models.TextField()
    number = models.TextField()
    city = models.TextField()
    postal_code = models.TextField(null=True, blank=True)
    country = models.TextField()
    user = models.ForeignKey(
        "auth.User", related_name="addresses", on_delete=models.CASCADE
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["street", "number", "city", "country", "user"],
                name="users__addresses_unique",
            )
        ]

    @classmethod
    def is_duplicate(
        cls, street: str, number: str, city: str, country: str, user_id: int
    ) -> bool:
        duplicates = cls.objects.filter(
            street=street,
            number=number,
            city=city,
            country=country,
            user_id=user_id,
        )
        return len(duplicates) > 0

    def __str__(self) -> str:
        return (
            f"{self.street} {self.number}, "
            f"{self.postal_code + ' ' if self.postal_code else ''}{self.city}, "
            f"{self.country}"
        )
