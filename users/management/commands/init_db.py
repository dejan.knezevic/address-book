"""
Database initialization command.
This is to be used only for local manual testing.

Can be improved by:
 - adding parameters for specific operation or creating specific instances.
 - using a configuration file for data that will be inserted if non-random data is needed
"""
import random
from typing import Any

from django.contrib.auth.models import User
from django.core.management import BaseCommand
from faker import Faker

from users.models import Address

fake = Faker()


def reset_users() -> None:
    """
    Recreate test users.
    Existing users will be deleted.
    """
    print("Initializing users...")
    User.objects.all().delete()
    User.objects.create_superuser(username="admin", password="admin")
    for i in range(1, 11):
        user = User.objects.create_user(username=fake.user_name(), password="password")
        user.save()


def reset_addresses() -> None:
    """
    Recreate test addresses.
    Existing addresses will be deleted.
    """
    print("Initializing addresses...")
    users = User.objects.all()
    for user in users:
        n = random.randint(1, 10)
        for i in range(n):
            address = Address(
                street=fake.street_name(),
                number=f"{fake.random_digit_not_null()}",
                city=fake.city(),
                postal_code=fake.postcode(),
                country=fake.country(),
                user=user,
            )
            address.save()


class Command(BaseCommand):
    """
    Management command for initializing the database with test data.
    Only to be used for local testing.
    """

    help = "Initialize database with test data"

    def handle(self, *args: Any, **options: Any) -> None:
        reset_users()
        reset_addresses()
