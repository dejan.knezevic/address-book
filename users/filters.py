from django.utils.translation import gettext_lazy as _
from django_filters import CharFilter, FilterSet


class AddressFilter(FilterSet):
    street = CharFilter(help_text=_("Street name"))
    number = CharFilter(help_text=_("House/Apartment number"))
    city = CharFilter(help_text=_("City name"))
    postal_code = CharFilter(help_text=_("Area postal code"))
    country = CharFilter(help_text=_("Country name"))
