"""
Module used for address views.

A ModelViewSet is used to cover all required CRUD operations, as well as an additional
bulk deletion view.

These views are only available to logged-in users, and they can only
perform operations on their own addresses.
"""
from typing import Any, Type

from django.db.models import QuerySet
from django.utils.translation import gettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import authentication, permissions, status, viewsets
from rest_framework.decorators import action
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.serializers import BaseSerializer

from address_book.errors import BadRequest
from users.filters import AddressFilter
from users.models import Address
from users.serializers import (
    AddressBulkDeleteSerializer,
    AddressCreateSerializer,
    AddressRetrieveSerializer,
    AddressSerializer,
)


@extend_schema_view(
    list=extend_schema(
        description=_("Retrieve a paginated list of current user's addresses.")
    ),
    create=extend_schema(description=_("Add a new address to current user.")),
    retrieve=extend_schema(description=_("Retrieve a current user's address by id.")),
    update=extend_schema(description=_("Update current user's entire address.")),
    partial_update=extend_schema(
        description=_("Update a current user's address properties.")
    ),
    destroy=extend_schema(description=_("Delete a current user's address by id.")),
    bulk_delete=extend_schema(
        description=_("Delete multiple addresses that belong to current user.")
    ),
)
class AddressViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes = [authentication.TokenAuthentication]
    filter_backends = [DjangoFilterBackend]
    filterset_class = AddressFilter

    def get_queryset(self) -> QuerySet[Address]:
        """Return a list of all the addresses for the currently authenticated user"""
        return Address.objects.filter(user_id=self.request.user.id)  # type: ignore

    def get_serializer_class(self) -> Type[BaseSerializer[Any]]:
        """Set serializer depending on address operation"""
        if self.action in ["retrieve", "list"]:
            return AddressRetrieveSerializer
        elif self.action == "create":
            return AddressCreateSerializer
        elif self.action == "bulk_delete":
            return AddressBulkDeleteSerializer
        return AddressSerializer

    def perform_create(self, serializer: BaseSerializer[Any]) -> None:
        """Check for duplicate addresses before inserting"""
        if Address.is_duplicate(
            street=serializer.validated_data.get("street"),
            number=serializer.validated_data.get("number"),
            city=serializer.validated_data.get("city"),
            country=serializer.validated_data.get("country"),
            user_id=self.request.user.id,  # type: ignore
        ):
            raise BadRequest(
                detail=_("Address is a duplicate"), code="address.duplicate"
            )

        serializer.save(user=self.request.user)

    @action(detail=False, methods=["POST"])
    def bulk_delete(self, request: Request) -> Response:
        ids = request.data.get("ids")
        addresses = Address.objects.filter(id__in=ids)
        addresses.delete()
        return Response(status=status.HTTP_200_OK)
