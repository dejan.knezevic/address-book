from rest_framework import serializers

from users.models import Address


class AddressSerializer(serializers.ModelSerializer[Address]):
    class Meta:
        model = Address
        fields = ["street", "number", "city", "postal_code", "country"]


class AddressRetrieveSerializer(serializers.ModelSerializer[Address]):
    class Meta:
        model = Address
        fields = ["id", "street", "number", "city", "postal_code", "country"]


class AddressCreateSerializer(serializers.ModelSerializer[Address]):
    class Meta:
        model = Address
        fields = ["id", "street", "number", "city", "postal_code", "country"]
        read_only_fields = ["id"]


class AddressBulkDeleteSerializer(serializers.Serializer[Address]):
    ids = serializers.ListField(child=serializers.UUIDField())
