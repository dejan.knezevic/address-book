"""
Admin site is enabled for ease of use during examination.

If it is necessary to exist in production,
its url can be allowed only on private networks for added security.
"""
from django.contrib import admin

from .models import Address

admin.site.register(Address)
