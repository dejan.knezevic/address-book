"""
Views for login and logout operations.

Using username/password combination a user can log in by creating or retrieving a token.
Logging out entails removal of the user token.

Additional notes:
- Token expiration can be implemented for added security
- Scopes and different token types can be implemented to limit operations that can be performed
    by this specific session
"""
from typing import Any

from rest_framework import authentication, permissions, status
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from authentication.serializers import TokenLoginSerializer


class LoginView(ObtainAuthToken):
    """Log in user by creating a new token or retrieve existing token for logged-in users"""

    serializer_class = TokenLoginSerializer

    def post(self, request: Request, *args: Any, **kwargs: Any) -> Response:
        data = request.data
        serializer = self.serializer_class(data=data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        token, created = Token.objects.get_or_create(user=user)
        return Response(
            {
                "token": token.key,
            }
        )


class LogoutView(APIView):
    """Log out user by deleting token"""

    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = None

    @staticmethod
    def post(request: Request) -> Response:
        token = Token.objects.get(user=request.user)  # type: ignore
        token.delete()
        return Response(status=status.HTTP_200_OK)
