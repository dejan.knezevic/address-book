"""
This module is used for all service specific errors.

If multiple services of the same type would be made,
this could easily be extracted into its own library.
"""
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import APIException


class BadRequest(APIException):
    status_code = 400
    default_detail = _("Bad request")
    default_code = "bad_request"
